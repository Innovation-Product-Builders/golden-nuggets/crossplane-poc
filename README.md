# Crossplane POC
Build control planes without needing to write code. Crossplane has a highly extensible backend that enables you to orchestrate applications and infrastructure no matter where they run, and a highly configurable frontend that lets you define the declarative API it offers.

We can create any API endpoint via kubernetes native way to provision infrastructure at any cloud provider(GCP,AWS,Azure etc) from a kubernetes cluster.

## Prerequisites
- A kubernetes cluster where we can deploy crossplane backend
- Pre-installed helm in the cluster

## Crossplane installation guide

In my case I am using my personal PC to run kubernetes cluster locally and install crossplane backend in this cluster to provision IPB existing infrastructure(now its provisioning with Terraform) by crossplane API endpoint in Google cloud platform.

**You can follow the installation process from the official [page](https://docs.crossplane.io/v1.12/getting-started/provider-gcp/)**

Verify crossplane is correctly deployed

```
kubectl get pods -n crossplane-system
NAME                                      READY   STATUS    RESTARTS       AGE
crossplane-65867555f7-ffg9j               1/1     Running   6 (35m ago)    11d
crossplane-rbac-manager-57f7949747-j8qrd  1/1     Running   6 (35m ago)    11d

# check all default CRDs of crossplane
kubectl get crds | grep "crossplane"   

compositeresourcedefinitions.apiextensions.crossplane.io           
compositionrevisions.apiextensions.crossplane.io                   
compositions.apiextensions.crossplane.io                           
configurationrevisions.pkg.crossplane.io                           
configurations.pkg.crossplane.io                                   
controllerconfigs.pkg.crossplane.io                                
environmentconfigs.apiextensions.crossplane.io                     
locks.pkg.crossplane.io                                            
providerrevisions.pkg.crossplane.io                                
providers.pkg.crossplane.io                                        
storeconfigs.secrets.crossplane.io                                 
```
## Create GCP upbound provider 
```
kubectl create -f gcp-provider.yaml
```
Verify provider and new gcp CRDs are availble

```
kubectl get pods -n crossplane-system | grep "gcp"
upbound-provider-gcp-95821cfe9570-558454b844-c2m4d   1/1     Running   11 (46m ago)   11d

#Check CRDs
kubectl get crds | grep "gcp.upbound.io" | wc -l
332 # all available CRDs installed for GCP
```
Authenticate GCP from my kubernetes cluster.

- Need to create a service account with appropiate role
- Create a JSON key credentials file for this service account

Create a kubernetes secrets with this JSON key file to use it for the purpose of authentication.

```
kubectl create secret \                                               
generic gcp-secret \
-n crossplane-system \
--from-file=creds=./service-account.json
# check the secret
kubectl get secret -n crossplane-system                   
NAME                     TYPE                 DATA   AGE
gcp-secret               Opaque               1      11d
```
Now we can use this secrets to make proper authentication to the GCP project with ProviderConfig.

```
kubectl apply -f gcp-provider-config.yaml
#check
kubectl get providerconfig                                            
NAME                                                AGE
providerconfig.gcp.upbound.io/gcp-provider-config   11d
```


We are using [upbound](https://marketplace.upbound.io) market place provider to provision different types of resource in Google cloud.

We can create re-usable API endpoint for provisioning resource in cloud platform by a combination of Composition,CompositeResourceDefinition to make a dynamically configurable endpoint to provision resource via Claim within any namespace by any developer/OPS guy.

**Composition:** In the compostion file we put all resource definition what actually we want to provision from a single API call by a Claim request.

**CompositeResourceDefinition:** This file is responsible for defining API schema which can be dynamically configured from claim request providing as parameters value.

**Claim:** In the CompositeResourceDefinition(XRD) we have define the dynamic API endpoint which can be easily call/use by any one via a claim within a namespace. So the same API endpoint can create same type of different resources in cloud platform which will be totally isolataed from each other by namespace.  

We can provide RBAC role to different team in a big organisation via different namespace. Now team member can easily create resources on cloud by using API endpoint in their allow namepace only.By creating a claim request by providing his expected resource parameters in claim.

By this approach we can easily maintain our cloud infrastructure as a loosely coupled unit where resource provision for a team is not dependent on DevOps/OPS team rather developer can do it.

Now we will create some XRD API endpoints to provision resource in GCP such as storage bucket,GKE cluster,Node etc.

### Create Different XRD's 
#### upbound-storage-provider
In this directory you will find 3 files  for composition,CompositeResourceDefinition and claim to use the CompositeResourceDefinition or XRD API example.
- **composition-bucket.yaml** file contains the bucket resource defintion by using upbound crds. In this composition we set the dynamic options for bucket loaction(US or EU).
- **xrd-bucket.yaml** file contains the API schema definitions which will be called from claim with providing required parameters. Any team can create his own bucket by creating separate claim per namespace.
- **xrd-claim.yaml** Example of a claim to use the above XRD to provision storage bucket in GCP.

Here is the example for creating a bucket in US for the development team in namespace **dev-team**
```
apiVersion: custom-api.example.org/v1alpha1
kind: Bucket
metadata:
  name: bucket-dev-team
  namespace: dev-team
spec:
  location: "US"
```
Now create  storage resource composition and XRD
```
cd upbound-storage-provider
kubectl apply -f composition-bucket.yaml
kubectl apply -f xrd-bucket.yaml

#Verify obejcts
kubectl get composition                         
NAME           XR-KIND        XR-APIVERSION                     AGE
bucket         XBucket        custom-api.example.org/v1alpha1   10m

kubectl get xrd                        
NAME                                   ESTABLISHED   OFFERED   AGE
xbuckets.custom-api.example.org        True          True      11m

```
We have already defined claim name for the XRD definition, we can find it by below command
```
kubectl get  xrd xbuckets.custom-api.example.org -o=yaml

apiVersion: apiextensions.crossplane.io/v1
kind: CompositeResourceDefinition
metadata:
  .......
spec:
  claimNames:
    kind: Bucket 
    plural: buckets
```

#### upbound-container-provider
In this directory you will find 3 files  for composition,CompositeResourceDefinition and claim to use the CompositeResourceDefinition or XRD API example.
- **composition-gke.yaml** file contains the gke cluster and nodepool resource defintion by using upbound crds. In this composition we set the dynamic options for cluster and nodepool  *loaction,machineType and nodeCount*.
- **xrd-gke.yaml file** contains the API schema definitions which will be called from claim with providing required parameters. Any team can create his own bucket by creating separate claim per namespace.
- **xrd-claim.yaml** Example of a claim to use the above XRD to provision GKE cluster and nodepool in GCP.

Here is the example for creating a GKE cluster and nodepool  for the development team in namespace **test**.
```
apiVersion: custom-api.example.org/v1alpha1
kind: GkeNodepool
metadata:
  name: claimed-gek-nodepool
  namespace: test
spec:
  location: "us-central1-a"
  nodeCount: 2
  machineType: "e2-small"
```
Now create  GKE resource composition and XRD
```
cd upbound-container-provider
kubectl apply  -f composition-gke.yaml
kubectl apply -f xrd-gke.yaml

#Verify obejcts                         
kubectl get composition         
NAME                        XR-KIND        XR-APIVERSION                     AGE
bucket                      XBucket        custom-api.example.org/v1alpha1   152m
gke-cluster-with-node-pool  XGkeNodepool   custom-api.example.org/v1alpha1   2m34s

kubectl get xrd        
NAME                                   ESTABLISHED   OFFERED   AGE
xbuckets.custom-api.example.org        True          True      153m
xgkenodepools.custom-api.example.org   True          True      2m36s

```
We have already defined claim name for the XRD definition, we can find it by below command
```
kubectl get  xrd xgkenodepools.custom-api.example.org -o=yaml

apiVersion: apiextensions.crossplane.io/v1
kind: CompositeResourceDefinition
metadata:
  .......
spec:
  claimNames:
    kind: GkeNodepool
    plural: gkenodepools
```
Now we can use above xrd for different claim requests per differenet team for their necessity where managed isolation via namespace.

Suppose dev-team wants to create a storage bucket in US region , they can easily do it by creating claim of kind **bucket**

Run the below command to create it

```
cd upbound-storage-provider
kubectl create ns dev-team
kubectl apply  -f xrd-claim.yaml
# check bucket
kubectl get bucket -n dev-team     
NAME              SYNCED   READY   CONNECTION-SECRET   AGE
bucket-dev-team   True     True                        2m23s

# Go to google console to check the bucket

# Delete the bucket by kubectl 
kubectl delete  -f xrd-claim.yaml

```
You can create as many buckets you want for different namespaces by simply create a separate claim per namespaces.

Now we can provision GKE cluster and Nodepool by XRD in **test** namespace.

```
cd upbound-container-provider
kubectl create ns test
kubectl apply  -f xrd-claim.yaml
# check cluster and nodepool
kubectl get cluster -n test                                  
NAME                               READY   SYNCED      AGE
claimed-gek-nodepool-l5gpc-sjfbj   True    True        37m

kubectl get nodepool -n test                                 
NAME                               READY   SYNCED    AGE
claimed-gek-nodepool-l5gpc-ltz5v   True    True      39m


# Go to google console to check the gke cluster and nodepool

# Delete the Cluster and NodePool by kubectl 
kubectl delete -f xrd-claim.yaml

```
You can create as many clusters and nodepool with various size,locations or machine type as you want for different namespaces by simply create a separate claim per namespaces.

## Terraform with Crossplane
Crossplane also provide a option to use Terraform as kubernetes native way which is highly requiured if no one can have available provider for any of the resources via crossplane or upbound provider yet. 

I have tried to accomplish above all operation(which are using XRD API) via upbound terraform provider with additinally configured a Identity Role as there is no upbound or crossplane provider to create custom identity role.

For the terraform we need to create sparate terraform provider and providerConfig to authenticate with GCP.

We will use the same secrets *gcp-secret* as JSON credentials file. In the provider config we will configure terraform remote backend to store state file with in a bucket.

**Here terraform is also reuasble by XRD via kubernetes native approach**

#### upbound-terraform-provider
In this directory you will find 5 files  for provider,providerConfig,composition,CompositeResourceDefinition and claim to use the CompositeResourceDefinition or XRD API example.
- **provider.yaml** file is used for setting up terraform upbound provider.
- **provider-config.yaml** file has the GCP authentication secrets value and configuration values for GCP terraform provider and terraform remote backend staff with a GCS bucket to store terraform state.
- **composition-gke.yaml** file contains the gke cluster , nodepool and custom IAM role resource defintion by using upbound crds. In this composition we set the dynamic options for cluster and nodepool  *clusterName,nodepoolName,loaction,machineType,nodeCount and diskSize*.
- **xrd-gke.yaml** file contains the API schema definitions which will be called from claim with providing required parameters. Any team can create his own bucket by creating separate claim per namespace.
- **xrd-claim.yaml** Example of a claim to use the above XRD to provision GKE cluster and nodepool in GCP.

Here is the example for creating a GKE cluster, nodepool and IAM role for the development team in namespace **test-dev**.
```
apiVersion: custom-api.example.org/v1alpha1
kind: TerraformGkeNodepool
metadata:
  name: terraform-gek-nodepool
  namespace: test-dev
spec: 
  clusterName: "mie-lab-cluster-test"
  location: "us-central1-a"
  nodeCount: "2"
  machineType: "e2-small"
  nodepoolName: "test-node-pool"
  diskSize: "50"

```

Now create composition and XRD for terraform provider.
```
# Create provider command
upbound-terraform-provider
kubectl apply -f provider.yaml 

#verify provider
kubectl get provider.pkg.crossplane.io
NAME               INSTALLED   HEALTHY             PACKAGE         AGE
provider-terraform True        True    provider-terraform:v0.8.0   38s

# Create provider config with setting up remote backend
kubectl apply -f provider-config.yaml

#verify provider config

kubectl get providerconfig.tf.upbound.io                          
NAME                        AGE
terraform-provider-config   1m
# Create composition
kubectl apply -f composition-terraform.yaml

#verify composition  
kubectl get composition                      
NAME           XR-KIND                 XR-APIVERSION                     AGE
terraform-gke  XTerraformGkeNodepool   custom-api.example.org/v1alpha1   3s

# Create XRD

kubectl create  -f xrd-terraform.yaml

# verify XRD
kubectl get xrd
NAME                                            ESTABLISHED   OFFERED   AGE
xterraformgkenodepools.custom-api.example.org   True          True      35s

```
We have already defined claim name for the XRD definition, we can find it by below command

```
kubectl get  xrd xgkenodepools.custom-api.example.org -o=yaml

apiVersion: apiextensions.crossplane.io/v1
kind: CompositeResourceDefinition
metadata:
  .......
spec:
  claimNames:
    kind: TerraformGkeNodepool
    plural: terraformgkenodepools
```
Now we are ready to use above XRD from claim.

```
kubectl create ns test-dev
kubectl apply  -f xrd-claim.yaml 

# check the claim 
kubectl get TerraformGkeNodepool -n test-dev
```
Now we can go to your Google cloud console, you will find all resources Cluster,nodepool and a role ipb-limited-access.

Destroy the resources which are created by the claim.

```
Kubectl delete -f xrd-claim.yaml
```
